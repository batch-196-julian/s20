function showMessage(){
	console.log("Kaya mo yan!! Wag ka sumuko. Laban lang!!")
}

// showMessage();
// showMessage();
// showMessage();
// showMessage();
// showMessage();

// While Loop - allows us to repeat a task/code while the condition is true.

/*
	while(condition)
	task;
	increment/decrement
*/

let count = 5
// while (count !==0){

// 	showMessage();
// 	count--;

// }

// while (count !==0){

// 	console.log(count);
// 	count--;

// }

// do {
// 	console.log(count);
// 	count--;

// }while (count === 0);

// while (count === 0){

// 	console.log(count);
// 	count--;
// }

// let counterMini = 20

// while (counterMini !== 0){

// 	console.log(counterMini);
// 	counterMini--;
// }

// for (let count = 0; count <=20; count++){

// 	console.log(count);
// }


// for (let x = 1; x < 10; x++){
// 	let sum = 1 + x
// 	console.log("The sum of " + 1 + " + "  + x + " = " + sum );
// }


// for(let counter = 0; counter<= 20; counter++){

// 	if (counter % 3 == 0){
// 		continue;
// 	}

// console.log(counter);
// /*
// 	Break - allows to end the execution of a loop.

// */
// if (counter === 2){
// 	break;
// }
// }


// for(let counter = 5; counter<= 100; counter++){

// 	if (counter % 5){
// 		continue;
// 	}

// console.log(counter);
// // /*
// // 	Break - allows to end the execution of a loop.

// // */
// if (counter === 100){
// 	break;
// }
// }

for (let count = 0; count <= 100; count++){
	if (count % 5 === 0){
		console.log(count);
	}

}